(deftemplate task
	(slot id (type INTEGER))
	(multislot hours (type INTEGER) (cardinality 8 8 ) (range 0 1))
	(slot assigned (type SYMBOL) (allowed-symbols NO YES)(default NO))
  (slot duration (type INTEGER)))

	(deftemplate day
	(slot id (type INTEGER) (range 1 22))
	(multislot schedule (type INTEGER)
  (cardinality 8 8) (default 0 0 0 0 0 0 0 0))
	(multislot contained-tasks (type INTEGER)))

	(defglobal
			 ?*time_invested* = 0
		 )
