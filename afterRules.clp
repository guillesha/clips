(defrule print_not_assigned_tasks
  (declare (salience -100))
    (task (id ?id) (assigned NO) )
	=>
    (printout t "The task " ?id
    " has not been assigned". crlf))


(defrule print_days
    (declare (salience -100))
    (day (id ?id)(contained-tasks $?contained)
    (schedule $?schedule))
    =>
    (printout t "The day " ?id
    " has the tasks " $?contained "." crlf))

(defrule print_free_time
    (declare (salience -100))
 =>
 (printout t "The free time has been "
 (- 176 ?*time_invested*) crlf))
