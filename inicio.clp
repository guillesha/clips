(deftemplate tarea
	(slot id (type INTEGER))
	(multislot horas (type INTEGER) (cardinality 8 8 ))
	(slot asignada (type SYMBOL) (allowed-symbols NO SI) (default NO))
	(slot duracion (type INTEGER))
	(slot inicio (type INTEGER)))

(deftemplate jornada
	(slot id (type INTEGER) (range 1 22))
	(multislot huecos (type INTEGER) (cardinality 8 8) (default 0 0 0 0 0 0 0 0))
	(multislot tareas_contenidas (type INTEGER)))


	(deffacts introducir_tareas
		(tarea (id 1) (horas 0 1 1 1 0 0 0 0))
		(tarea (id 2) (horas 0 0 0 0 0 0 1 1))
		(tarea (id 3) (horas 0 0 1 1 0 0 0 0))
		(tarea (id 4) (horas 0 0 0 0 1 1 1 0))
		(tarea (id 5) (horas 0 0 1 1 0 0 0 0))
		(tarea (id 6) (horas 0 1 1 1 0 0 0 0))
		(tarea (id 7) (horas 0 1 1 1 1 0 0 0))
		(tarea (id 8) (horas 1 1 0 0 0 0 0 0))
		(tarea (id 9) (horas 0 0 0 0 0 0 0 1))
		(tarea (id 10) (horas 0 0 0 0 0 1 1 0))
		(tarea (id 11) (horas 0 1 1 1 0 0 0 0))
		(tarea (id 12) (horas 0 0 0 0 0 1 1 1))
		(tarea (id 13) (horas 0 0 0 0 0 1 0 0))
		(tarea (id 14) (horas 0 1 1 1 0 0 0 0))
		(tarea (id 15) (horas 0 0 0 0 0 0 1 1))
		(tarea (id 16) (horas 1 1 1 1 0 0 0 0))
		(tarea (id 17) (horas 0 0 0 0 1 1 1 0))
		(tarea (id 18) (horas 1 0 0 0 0 0 0 0))
		(tarea (id 19) (horas 1 1 0 0 0 0 0 0))
		(tarea (id 20) (horas 0 1 1 1 0 0 0 0))
		(tarea (id 21) (horas 0 0 0 0 0 0 1 1))
		(tarea (id 22) (horas 0 0 0 0 0 0 1 1))
		(tarea (id 23) (horas 0 0 0 1 1 0 0 0))
		(tarea (id 24) (horas 0 0 0 0 0 0 1 0))
		(tarea (id 25) (horas 0 0 0 0 0 0 1 1))
		(tarea (id 26) (horas 0 1 1 1 0 0 0 0))
		(tarea (id 27) (horas 0 0 0 0 1 0 0 0))
		(tarea (id 28) (horas 0 0 0 0 0 0 0 1))
		(tarea (id 29) (horas 0 0 1 1 1 1 1 0))
		(tarea (id 30) (horas 0 1 1 1 0 0 0 0))
		(tarea (id 31) (horas 0 0 0 0 1 1 1 1))
		(tarea (id 32) (horas 0 0 0 1 1 1 1 1))
		(tarea (id 33) (horas 0 0 0 1 1 0 0 0))
		(tarea (id 34) (horas 0 0 0 0 0 0 1 0))
		(tarea (id 35) (horas 0 0 0 0 1 1 1 1))
		(tarea (id 36) (horas 0 1 1 1 0 0 0 0))
		(tarea (id 37) (horas 0 0 0 0 1 0 0 0))
		(tarea (id 38) (horas 0 0 0 0 0 0 0 1))
		(tarea (id 39) (horas 0 0 1 1 1 1 1 0))
		(tarea (id 40) (horas 1 1 1 1 0 0 0 0))
		(tarea (id 41) (horas 1 1 1 1 0 0 0 0))
		(tarea (id 42) (horas 1 1 1 1 1 0 0 0))
		(tarea (id 43) (horas 0 0 0 1 1 0 0 0))
		(tarea (id 44) (horas 1 0 0 0 0 0 0 0))
		(tarea (id 45) (horas 0 1 1 1 1 0 0 0))
		(tarea (id 46) (horas 0 1 1 1 0 0 0 0))
		(tarea (id 47) (horas 1 0 0 0 0 0 0 0))
		(tarea (id 48) (horas 1 0 0 0 0 0 0 0))
		(tarea (id 49) (horas 0 0 1 1 1 1 1 0))
		(tarea (id 50) (horas 1 1 1 0 0 0 0 0))

	)

	(deffacts introducir_jornadas
		(jornada (id 1) )
		(jornada (id 2) )
		(jornada (id 3) )
		(jornada (id 4) )
		(jornada (id 5) )
		(jornada (id 6) )
		(jornada (id 7) )
		(jornada (id 8) )
		(jornada (id 9) )
		(jornada (id 10) )
		(jornada (id 11) )
		(jornada (id 12) )
		(jornada (id 13) )
		(jornada (id 14) )
		(jornada (id 15) )
		(jornada (id 16) )
		(jornada (id 17) )
		(jornada (id 18) )
		(jornada (id 19) )
		(jornada (id 20) )
		(jornada (id 21) )
		(jornada (id 22) )
		)

;reglas pra calcular el inicio
	(defrule asignar_inicio_1
		(declare (salience 500))
		  ?f1 <- (tarea  (inicio 0) (horas ?d1&:(= ?d1 1) $? ))
		   =>
		(modify ?f1 (inicio 1))
			 )

		(defrule asignar_inicio_2
				(declare (salience 500))
	 			  ?f1 <- (tarea  (inicio 0) (horas ? ?d1&:(= ?d1 1) $? ))
	 			   =>
	 			(modify ?f1 (inicio 2))
	 			)
				(defrule asignar_inicio_3
						(declare (salience 500))
							?f1 <- (tarea  (inicio 0) (horas ? ? ?d1&:(= ?d1 1) $? ))
							 =>
						(modify ?f1 (inicio 3))
						)
						(defrule asignar_inicio_4
								(declare (salience 500))
					 			  ?f1 <- (tarea  (inicio 0) (horas ? ? ? ?d1&:(= ?d1 1) $? ))
					 			   =>
					 			(modify ?f1 (inicio 4))
					 			)
								(defrule asignar_inicio_5
										(declare (salience 500))
							 			  ?f1 <- (tarea  (inicio 0) (horas ? ? ? ? ?d1&:(= ?d1 1) $? ))
							 			   =>
							 			(modify ?f1 (inicio 5))
							 			)
									(defrule asignar_inicio_6
											(declare (salience 500))
									 			  ?f1 <- (tarea  (inicio 0) (horas ? ? ? ? ? ?d1&:(= ?d1 1) $? ))
									 			   =>
									 			(modify ?f1 (inicio 6))
									 			)
												(defrule asignar_inicio_7
														(declare (salience 500))
											 			  ?f1 <- (tarea  (inicio 0) (horas ? ? ? ? ? ? ?d1&:(= ?d1 1) ? ))
											 			   =>
											 			(modify ?f1 (inicio 7))
											 			)
														(defrule asignar_inicio_8
																(declare (salience 500))
																	?f1 <- (tarea  (inicio 0) (horas $? ?d1&:(= ?d1 1) ))
																	 =>
																(modify ?f1 (inicio 8))
																)

; regla para definir la duración, se suman todos los componetes de la hora
(defrule definir_duracion
		(declare (salience 500))
	  ?f1 <- (tarea  (duracion 0) (horas ?h1 ?h2 ?h3 ?h4 ?h5 ?h6 ?h7 ?h8))
		=>
			(modify ?f1 (duracion (+ ?h1 (+ ?h2 (+ ?h3 (+ ?h4 (+ ?h5 (+ ?h6 (+ ?h7 ?h8)))))))))
	)

; aplicación menor inicio

		(defglobal
			   ?*tiempo_invertido* = 0
			 )

	(defrule asignar_tarea
			?f1<-(jornada (id ?id_jornada) (huecos ?h1 ?h2 ?h3 ?h4 ?h5 ?h6 ?h7 ?h8)    (tareas_contenidas $?contents))
			?f2<-(tarea (id ?id) (inicio ?inicio) (horas ?d1&:(= (* ?h1 ?d1) 0) ?d2&:(= (* ?h2 ?d2) 0)
			?d3&:(= (* ?h3 ?d3) 0) ?d4&:(= (* ?h4 ?d4) 0) ?d5&:(= (* ?h5 ?d5) 0) ?d6&:(= (* ?h6 ?d6) 0)
			?d7&:(= (* ?h7 ?d7) 0) ?d8&:(= (* ?h8 ?d8) 0) ) (asignada NO) (duracion ?duracion))
			(not  (tarea (asignada NO) (duracion ?duracion2&:(> ?duracion2 ?duracion)) (inicio ?inicio2&:(< ?inicio2 ?inicio))  (horas ?t1&:(= (* ?h1 ?t1) 0) ?t2&:(= (* ?h2 ?t2) 0)
			?t3&:(= (* ?h3 ?t3) 0) ?t4&:(= (* ?h4 ?t4) 0) ?t5&:(= (* ?h5 ?t5) 0) ?t6&:(= (* ?h6 ?t6) 0)
			?t7&:(= (* ?h7 ?t7) 0) ?t8&:(= (* ?h8 ?t8) 0) )  ))
		 =>
		 (modify ?f2  (asignada SI))
		 (modify ?f1  (tareas_contenidas $?contents ?id) (huecos (+ ?h1 ?d1) (+ ?h2 ?d2) (+ ?h3 ?d3)
		 (+ ?h4 ?d4) (+ ?h5 ?d5) (+ ?h6 ?d6) (+ ?h7 ?d7) (+ ?h8 ?d8)))
		 (bind ?*tiempo_invertido* (+ ?*tiempo_invertido* ?duracion))
		 )

		(defrule imprime_jornadas
			(jornada (id ?id_jornada)(tareas_contenidas $?contenidas) (huecos $?huecos))
			=>
			(printout t "La jornada " ?id_jornada  " tiene las tareas " $?contenidas "." crlf)
			)

		(defrule imprime_tareas_no_asignadas
		(tarea (id ?id) (asignada NO) )
				=>
				(printout t crlf)
				(printout t "La tarea " ?id  " no ha sido asignada." crlf)
				)

		(defrule imprime_tiempo
			=>
				(printout t "El tiempo libre ha sido " (- 176 ?*tiempo_invertido*) crlf))
