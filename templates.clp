(deftemplate task
	(slot id (type INTEGER))
	(multislot hours (type INTEGER) (cardinality 8 8 ))
	(slot assigned (type SYMBOL) (allowed-symbols NO YES)
    (default NO))
    (slot duration (type integer) (range 1 5)))
