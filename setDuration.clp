(defrule initialize_duration
   ; give more priority so it is
    ?f1 <- (task  (duration 0) (hours ?h1
    ?h2 ?h3 ?h4 ?h5 ?h6 ?h7 ?h8))
	=>
	(modify ?f1 (duration (+ ?h1
    ?h2  ?h3  ?h4  ?h5  ?h6  ?h7 ?h8)))	)
