(deftemplate tarea
	(slot id (type INTEGER))
	(multislot horas (type INTEGER) (cardinality 8 8 ))
	(slot asignada (type SYMBOL) (allowed-symbols NO SI) (default NO))
	(slot duracion (type INTEGER)))

(deftemplate jornada
	(slot id (type INTEGER) (range 1 22))
	(multislot huecos (type INTEGER) (cardinality 8 8) (default 0 0 0 0 0 0 0 0))
	(multislot tareas_contenidas (type INTEGER)))


	(deffacts introducir_tareas
		(tarea (id 1) (horas 0 0 1 1 1 0 0 0) (duracion 3))
		(tarea (id 3) (horas 0 0 0 0 3 3 0 0) (duracion 2))
		(tarea (id 2) (horas 0 0 0 0 0 0 2 2) (duracion 2))
		(tarea (id 4) (horas 4 4 4 4 0 0 0 0) (duracion 4)))

	(deffacts introducir_jornadas
			(jornada (id 1) )
			)

		(defglobal
			   ?*tiempo_invertido* = 0
			 )

	(defrule asignar_tarea
			?f1<-(jornada (id ?id_jornada) (huecos ?h1 ?h2 ?h3 ?h4 ?h5 ?h6 ?h7 ?h8)    (tareas_contenidas $?contents))
			?f2<-(tarea (id ?id) (horas ?d1&:(= (* ?h1 ?d1) 0) ?d2&:(= (* ?h2 ?d2) 0)
			?d3&:(= (* ?h3 ?d3) 0) ?d4&:(= (* ?h4 ?d4) 0) ?d5&:(= (* ?h5 ?d5) 0) ?d6&:(= (* ?h6 ?d6) 0)
			?d7&:(= (* ?h7 ?d7) 0) ?d8&:(= (* ?h8 ?d8) 0) ) (asignada NO) (duracion ?duracion))
			(not  (tarea (duracion ?duracion2&:(> ?duracion2 ?duracion))  (horas ?t1&:(= (* ?h1 ?t1) 0) ?t2&:(= (* ?h2 ?t2) 0)
			?t3&:(= (* ?h3 ?t3) 0) ?t4&:(= (* ?h4 ?t4) 0) ?t5&:(= (* ?h5 ?t5) 0) ?t6&:(= (* ?h6 ?t6) 0)
			?t7&:(= (* ?h7 ?t7) 0) ?t8&:(= (* ?h8 ?t8) 0) )  ))
		 =>
		 (modify ?f2  (asignada SI))
		 (modify ?f1  (tareas_contenidas $?contents ?id) (huecos (+ ?h1 ?d1) (+ ?h2 ?d2) (+ ?h3 ?d3)
		 (+ ?h4 ?d4) (+ ?h5 ?d5) (+ ?h6 ?d6) (+ ?h7 ?d7) (+ ?h8 ?d8)))
		 (bind ?*tiempo_invertido* (+ ?*tiempo_invertido* ?duracion))
		 )

		(defrule imprime_jornadas
			(jornada (id ?id_jornada)(tareas_contenidas $?contenidas) (huecos $?huecos))
			=>
			(printout t "La jornada " ?id_jornada  " tiene las tareas " $?contenidas "." crlf)
			)

		(defrule imprime_tareas_no_asignadas
		(tarea (id ?id) (asignada NO) )
				=>
				(printout t crlf)
				(printout t "La tarea " ?id  " no ha sido asignada." crlf)
				)

		(defrule imprime_tiempo
			=>
				(printout t "El tiempo libre ha sido " (- 176 ?*tiempo_invertido*) crlf))
