(deftemplate tarea
	(slot id (type INTEGER))
	(multislot horas (type INTEGER) (cardinality 8 8 ))
	(slot asignada (type SYMBOL) (allowed-symbols NO SI) (default NO))
	(slot duracion (type INTEGER))
	(slot inicio (type INTEGER))
	(slot tolerancia (type SYMBOL) (allowed-symbols NO ANTES DESPUES) (default NO)))


			(defglobal
				   ?*tiempo_invertido* = 0
				 )


(deftemplate jornada
	(slot id (type INTEGER) (range 1 22))
	(multislot huecos (type INTEGER) (cardinality 8 8) (default 0 0 0 0 0 0 0 0))
	(multislot tareas_contenidas (type INTEGER)))

	(deffacts introducir_tareas

	  (tarea (id 1) (horas 0 0 1 1 1 0 0 0) (duracion 3) (tolerancia ANTES))
		(tarea (id 2) (horas 0 0 0 0 1 0 0 0) (duracion 1) (tolerancia NO))

	 )

	(deffacts introducir_jornadas
	    (jornada (id 1) )
	    )

	(defrule asignar_tarea_sin_tolerancia
					(declare (salience 200))
					?f1<-(jornada (id ?id_jornada) (huecos ?d1 ?d2 ?d3 ?d4 ?d5 ?d6 ?d7 ?d8)    (tareas_contenidas $?contents))
					?f2<- (tarea (id ?id) (inicio ?inicio) (tolerancia ?tolerancia) (horas ?h1 ?h2
					?h3 ?h4 ?h5 ?h6 ?h7 ?h8)  (asignada NO) (duracion ?duracion))
					(test (eq ?tolerancia NO) )
					(test (and (= (* ?h1 ?d1) 0) (= (* ?h2 ?d2) 0) (= (* ?h3 ?d3) 0) (= (* ?h4 ?d4) 0) (= (* ?h5 ?d5) 0)
					(= (* ?h6 ?d6) 0) (= (* ?h7 ?d7) 0) (= (* ?h8 ?d8) 0) ))
				 =>
				 (modify ?f2  (asignada SI))
				 (modify ?f1  (tareas_contenidas $?contents ?id) (huecos (+ ?h1 ?d1) (+ ?h2 ?d2) (+ ?h3 ?d3)
				 (+ ?h4 ?d4) (+ ?h5 ?d5) (+ ?h6 ?d6) (+ ?h7 ?d7) (+ ?h8 ?d8)))
				 (bind ?*tiempo_invertido* (+ ?*tiempo_invertido* ?duracion))
				 )

		 (defrule asignar_tarea_con_tolerancia_antes
			  (declare (salience 100))
 				?f1<-(jornada (id ?id_jornada) (huecos ?d1 ?d2 ?d3 ?d4 ?d5 ?d6 ?d7 ?d8)    (tareas_contenidas $?contents))
 				?f2<- (tarea (id ?id) (inicio ?inicio) (tolerancia ANTES) (horas ?h1 ?h2
 				?h3 ?h4 ?h5 ?h6 ?h7 ?h8)  (asignada NO) (duracion ?duracion))
 				(test (and (= (* ?h2 ?d1) 0) (= (* ?h3 ?d2) 0) (= (* ?h4 ?d3) 0) (= (* ?h5 ?d4) 0) (= (* ?h6 ?d5) 0)
	 	 	  (= (* ?h7 ?d6) 0) (= (* ?h8 ?d7) 0) ))
 			 =>
 			 (modify ?f2  (asignada SI))
 			 (modify ?f1  (tareas_contenidas $?contents ?id) (huecos (+ ?h2 ?d1) (+ ?h3 ?d2) (+ ?h4 ?d3)
 			 (+ ?h5 ?d4) (+ ?h6 ?d5) (+ ?h7 ?d6) (+ ?h8 ?d7) ?d8))
 			 (bind ?*tiempo_invertido* (+ ?*tiempo_invertido* ?duracion))
 			 )

			(defrule asignar_tarea_con_tolerancia_despues
			 	(declare (salience 100))
		 		?f1<-(jornada (id ?id_jornada) (huecos ?d1 ?d2 ?d3 ?d4 ?d5 ?d6 ?d7 ?d8)    (tareas_contenidas $?contents))
		 		?f2<- (tarea (id ?id) (inicio ?inicio) (tolerancia DESPUES) (horas ?h1 ?h2
		 		?h3 ?h4 ?h5 ?h6 ?h7 ?h8)  (asignada NO) (duracion ?duracion))
		 		(test	(and (= (* ?h1 ?d2) 0) (= (* ?h2 ?d3) 0) (= (* ?h3 ?d4) 0) (= (* ?h4 ?d5) 0)
			 	(= (* ?h5 ?d6) 0) (= (* ?h6 ?d7) 0) (= (* ?h7 ?d8) 0)))
		 	   =>
		 		(modify ?f2  (asignada SI))
		 		(modify ?f1  (tareas_contenidas $?contents ?id) (huecos  ?d1 (+ ?h1 ?d2) (+ ?h2 ?d3) (+ ?h3 ?d4)
		 		(+ ?h4 ?d5) (+ ?h5 ?d6) (+ ?h6 ?d6) (+ ?h7 ?d8) ))
		 		(bind ?*tiempo_invertido* (+ ?*tiempo_invertido* ?duracion))
		 		 )




		 (defrule imprime_jornadas
	 		(jornada (id ?id_jornada)(tareas_contenidas $?contenidas) (huecos $?huecos))
	 		=>
	 		(printout t "La jornada " ?id_jornada  " tiene las tareas " $?contenidas "." crlf)
	 		)

	 	(defrule imprime_tareas_no_asignadas
	 	(tarea (id ?id) (asignada NO) )
	 			=>
	 			(printout t crlf)
	 			(printout t "La tarea " ?id  " no ha sido asignada." crlf)
	 			)

	 	(defrule imprime_tiempo
	 		=>
	 			(printout t "El tiempo libre ha sido " (- 176 ?*tiempo_invertido*) crlf))
