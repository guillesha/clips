(defrule assign_task
	?f1<-(task (id ?id) (hours ?h1 ?h2 ?h3 ?h4 ?h5 ?h6 ?h7 ?h8) (assigned NO)  (duration ?duration))
	?f2<-(day (id ?id_day) (contained-tasks $?contents) (schedule ?d1 ?d2 ?d3 ?d4 ?d5 ?d6 ?d7 ?d8))
	(test (and (= (* ?h1 ?d1) 0) (= (* ?h2 ?d2) 0) (= (* ?h3 ?d3) 0) (= (* ?h4 ?d4) 0) (= (* ?h5 ?d5) 0)
	(= (* ?h6 ?d6) 0) (= (* ?h7 ?d7) 0) (= (* ?h8 ?d8) 0) ))
 =>
 (modify ?f1  (assigned YES))
 (modify ?f2  (contained-tasks $?contents ?id) (schedule (+ ?h1 ?d1) (+ ?h2 ?d2) (+ ?h3 ?d3)
  (+ ?h4 ?d4) (+ ?h5 ?d5) (+ ?h6 ?d6) (+ ?h7 ?d7) (+ ?h8 ?d8)))
 (bind ?*time_invested* (+ ?*time_invested* ?duration))
 )
